//package com.daksa.asymmetric;
//
///**
// *
// * @author Nafhul A
// */
//import java.io.IOException;
//import java.security.GeneralSecurityException;
//import org.junit.Assert;
//import org.junit.Test;
//
//public class RSACipherTest {
//
//	private final String privateKeyPathName = "D://temp//private.key";
//	private final String publicKeyPathName = "D://temp//public.key";
//	private final String transformation = "RSA/ECB/PKCS1Padding";
//	private final String encoding = "UTF-8";
//
//	@Test
//	public void testEncryptDecryptWithKeyPairFiles() throws Exception {
//		try {
//			RSAKeyPair rsaKeyPair = new RSAKeyPair(2048);
//			rsaKeyPair.toFileSystem(privateKeyPathName, publicKeyPathName);
//
//			RSACipher rsaCipher = new RSACipher();
//			String encrypted = rsaCipher.encrypt("John", publicKeyPathName, transformation, encoding);
//			String decrypted = rsaCipher.decrypt(encrypted, privateKeyPathName, transformation, encoding);
//			System.out.println(decrypted);
//			Assert.assertEquals("John", decrypted);
//
//		} catch (GeneralSecurityException | IOException exception) {
//			Assert.fail("The testEncryptDecryptWithKeyPairFiles() test failed because: " + exception.getMessage());
//		}
//	}
//}
