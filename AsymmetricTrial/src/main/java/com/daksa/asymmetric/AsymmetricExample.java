/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.asymmetric;

/**
 *
 * @author Nafhul A
 */
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.Signature;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import javax.crypto.Cipher;
import javax.xml.bind.DatatypeConverter;

public class AsymmetricExample {

	public static void main(String[] args) throws Exception {
		
		// GENERATE KEY PAIR
		Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
		SecureRandom random = SecureRandom.getInstanceStrong();
		KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA", "BC");
		kpg.initialize(2048, random);
		KeyPair keyPair = kpg.genKeyPair();
		Key pubKey = keyPair.getPublic();
		Key privKey = keyPair.getPrivate();
		
		String jsonStringPurchase = "{\"mmsGroupId\":\"501\",\"username\":\"510015100151002B\",\"password\":\"123456\",\"restPurchaseRequest\":{\"amount\":\"1\",\"merchantQvaId\":\"802001112\",\"qvaInvoice\":\"201512281725a\",\"note\":\"Purchase By Merchant\",\"cashierId\":\"\"}}";
		byte[] data = jsonStringPurchase.getBytes("UTF8");

		
		Cipher cipher = Cipher.getInstance("RSA/None/PKCS1Padding", "BC");

		//decode server public key
		byte[] byteKey = Base64.getDecoder().decode("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArc7sEjvwcthTvRlWEPWyw9V0eZWbgNaqzk1Gwhdxj58pYIRppkTCCA5qva4KAS8KyKQS22MJunc+Z74QvSKdOUXui/iQHiIvg+okTTDkvJc7ODjovW3pqf3m+JtnurZHhWohp3qmpJaiYhwwNTO1gXSc+L9ZtYRND8OCoiHsWibv3Nw51l+cRQH7u7s9wBblQUPyNXVccPXWcSsE674NUBREN2FScVC1+Rz9K0fneTNvltV2p3TkFZ1g7zuAntVGn815nASgagAvhieW5iRQ9imYlofvpWZ7xTAIPCJyztWEh1ez5Jc3n1gR5I2ztC2+0uXdG67qjoKeoBOoDUp9GwIDAQAB");
		X509EncodedKeySpec X509publicKey = new X509EncodedKeySpec(byteKey);
		KeyFactory kf = KeyFactory.getInstance("RSA");
		PublicKey publicKeyServer = kf.generatePublic(X509publicKey);

		//encrypt purchase message
		cipher.init(Cipher.ENCRYPT_MODE, publicKeyServer, random);
		byte[] cipherText = cipher.doFinal(data);
		String stringToStore = Base64.getEncoder().encodeToString(cipherText);

		//create signature
		Signature sig = Signature.getInstance("SHA1withRSA");
		sig.initSign(keyPair.getPrivate());
		sig.update(cipherText);
		byte[] signatureBytes = sig.sign();
		
		String encodedSignature = Base64.getEncoder().encodeToString(signatureBytes);
		byte[] decodedSignature = Base64.getDecoder().decode(encodedSignature);
		System.out.println("Singature:" + encodedSignature);

		String encodedPublicKeyCustomer = Base64.getEncoder().encodeToString(keyPair.getPublic().getEncoded());
		byte[] bytePublicCustomerKey = Base64.getDecoder().decode(encodedPublicKeyCustomer);
		X509EncodedKeySpec customerPublicKey = new X509EncodedKeySpec(bytePublicCustomerKey);
		kf = KeyFactory.getInstance("RSA", "BC");
		PublicKey publicKeyCustomer = kf.generatePublic(customerPublicKey);

		byte[] decodedData = Base64.getDecoder().decode(stringToStore);
		
		// VERIFY SIGNATURE
		Signature signature = Signature.getInstance("SHA1withRSA");
		signature.initVerify(publicKeyCustomer);
		signature.update(decodedData);		
		System.out.println(signature.verify(decodedSignature));
				
		// PRINT ALL
		String privKeyEncoded = Base64.getEncoder().encodeToString(privKey.getEncoded());
		System.out.println("pub = " + encodedPublicKeyCustomer);
		System.out.println("priv = " + privKeyEncoded);
		System.out.println("msg = " + stringToStore);
		System.out.println("pub key = " + pubKey.toString());
		System.out.println("priv key = " + privKey.toString());
		System.out.println("pub key encoded = " + DatatypeConverter.printHexBinary(pubKey.getEncoded()));
		System.out.println("priv key encoded = " + DatatypeConverter.printHexBinary(privKey.getEncoded()));
	}
}
