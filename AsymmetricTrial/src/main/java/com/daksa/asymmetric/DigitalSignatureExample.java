/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.asymmetric;

/**
 *
 * @author Nafhul A
 */
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.Signature;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import javax.crypto.Cipher;
import javax.xml.bind.DatatypeConverter;

public class DigitalSignatureExample {

	public static void main(String[] args) throws Exception {
		Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
		SecureRandom random = SecureRandom.getInstanceStrong();
		KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA", "BC");
		kpg.initialize(2048, random);
		KeyPair keyPair = kpg.genKeyPair();
		Key pubKey = keyPair.getPublic();
		Key privKey = keyPair.getPrivate();
		
		String jsonStringPurchase = "{\"mmsGroupId\":\"501\",\"username\":\"510015100151002B\",\"password\":\"123456\",\"restPurchaseRequest\":{\"amount\":\"1\",\"merchantQvaId\":\"802001112\",\"qvaInvoice\":\"201512281725a\",\"note\":\"Purchase By Merchant\",\"cashierId\":\"\"}}";
		
		byte[] data = jsonStringPurchase.getBytes("UTF8");

		Cipher cipher = Cipher.getInstance("RSA/None/PKCS1Padding", "BC");

		byte[] byteKey = Base64.getDecoder().decode("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArc7sEjvwcthTvRlWEPWyw9V0eZWbgNaqzk1Gwhdxj58pYIRppkTCCA5qva4KAS8KyKQS22MJunc+Z74QvSKdOUXui/iQHiIvg+okTTDkvJc7ODjovW3pqf3m+JtnurZHhWohp3qmpJaiYhwwNTO1gXSc+L9ZtYRND8OCoiHsWibv3Nw51l+cRQH7u7s9wBblQUPyNXVccPXWcSsE674NUBREN2FScVC1+Rz9K0fneTNvltV2p3TkFZ1g7zuAntVGn815nASgagAvhieW5iRQ9imYlofvpWZ7xTAIPCJyztWEh1ez5Jc3n1gR5I2ztC2+0uXdG67qjoKeoBOoDUp9GwIDAQAB");
		X509EncodedKeySpec X509publicKey = new X509EncodedKeySpec(byteKey);
		KeyFactory kf = KeyFactory.getInstance("RSA");
		PublicKey publicKeyServer = kf.generatePublic(X509publicKey);

		cipher.init(Cipher.ENCRYPT_MODE, publicKeyServer, random);
		byte[] cipherText = cipher.doFinal(data);
		String stringToStore = Base64.getEncoder().encodeToString(cipherText);

		Signature sig = Signature.getInstance("SHA1withRSA");
		sig.initSign(keyPair.getPrivate());
		sig.update(cipherText);
		byte[] signatureBytes = sig.sign();
		
		String encodedSignature = Base64.getEncoder().encodeToString(signatureBytes);
		byte[] decodedSignature = Base64.getDecoder().decode(encodedSignature);
		System.out.println("Singature:" + encodedSignature);

		String encodedPublicKeyCustomer = Base64.getEncoder().encodeToString(keyPair.getPublic().getEncoded());
		byte[] bytePublicCustomerKey = Base64.getDecoder().decode(encodedPublicKeyCustomer);
		X509EncodedKeySpec customerPublicKey = new X509EncodedKeySpec(bytePublicCustomerKey);
		kf = KeyFactory.getInstance("RSA", "BC");
		PublicKey publicKeyCustomer = kf.generatePublic(customerPublicKey);

		byte[] decodedData = Base64.getDecoder().decode(stringToStore);
//		String encodedPrivKeyServer = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCtzuwSO/By2FO9GVYQ9bLD1XR5lZuA1qrOTUbCF3GPnylghGmmRMIIDmq9rgoBLwrIpBLbYwm6dz5nvhC9Ip05Re6L+JAeIi+D6iRNMOS8lzs4OOi9bemp/eb4m2e6tkeFaiGneqaklqJiHDA1M7WBdJz4v1m1hE0Pw4KiIexaJu/c3DnWX5xFAfu7uz3AFuVBQ/I1dVxw9dZxKwTrvg1QFEQ3YVJxULX5HP0rR+d5M2+W1XandOQVnWDvO4Ce1UafzXmcBKBqAC+GJ5bmJFD2KZiWh++lZnvFMAg8InLO1YSHV7PklzefWBHkjbO0Lb7S5d0bruqOgp6gE6gNSn0bAgMBAAECggEARw/iDzZ4gcwWuKriOH7sPwtH2uojMRMTtNN9iZ+iK5Fsjmy08WkNV9PlKnY82u0L4KrMluwwLw/P8QSRvXKoi6Ee/hV7fwaFHM8fVnwWGVFPwhXqeoC0127a/2wyhfE+3rpc1ifceFCqRai2wRnKLQqrnzxFBmT4SOirwXRTOSwgnRaBMjG3cFpdyi3iBkC4pMaEZ5N8d1E3fGFHbQHwv+8p9r1AspjHphRi2aj2KCptJfyXJMYdGl9fysGAOHGaet2Be1no+jYbA+FnMfD/l1wrBhioEvEo/m897svPct/4/PuW65QrTVVI4qH/hn95/n5yEkaBywWEAHvC8rT3mQKBgQD/glJPNl2puJjQgf0/Wmz4WjPrwqG0N6oR70xinz8dEHxYEjGGyZpBTZf4aO21SuJMZ5lOnvZS2Dfr0ErF2vV8VZcMG5IUztQSG05FrMhHBfrz7jHYp+Pyk2zc6+1nw6cd7HTgu+XvCEzBQC9rB9fA4krlKEJ5/sd9XSgKpmkthwKBgQCuJGoA3DBw2hshV2uKZBIYCMSEnjjhrv63X9cqIpkUoZ5X/fI+jaAsZSnYYVyYiC0uwpHuXwXQs9ohMAK13760kbIolweHERs7FS78yrjcUHhNEFNHf/fDra0AFy6D8XoWzPtXZQGWE/A1SikMH+a9f47Wz4YLN4Z6PO6okOi4zQKBgQDI9V35cT//kfGW5PYi0uMr/f4iKqV2EeXQT07brnY8TNAPkNelNID/ZaDmPtjfIvOTtlOss3f3Npnc/akhrpIvZT30xuaBOR8w0osgX8HxonF7U1efGIU/L9aKd4D0gLy621QdwYsaEoQT5oClBLkbup+/VofaXZnXLuKkZbgJlwKBgQCmvfdCG5PGexC/PBWczXUFzl9dHKMadZUvwHUlwzsQpjAX0PDDNJjJ6ZPHvHING1coDuXseM/4pj2WyYHO5A6Wl2sfsn1itt58FCyJoRKg4exO3fLf44g5tXKRQ6EmdXGRo3SGmhCKw0ydugTTtzgY+qqqxMp0sxXwtadPgHFVgQKBgD9QcGO2AECgU7PKkOHXCoEem8REj7WfgKJ7/D8p5dEeE5kH2abGlfKjkF0PXr2NNCF4mcjFn0/Vq5SeeVjZraRElE/KImVc8loF6t70vP1HPRvRQBvlWILdtt2Ox9TNr2cw27hnYLwySsb3mNUhGNX7e3H5JSnP/L3WUX2GTbXi";
//		byte[] clear = Base64.getDecoder().decode(encodedPrivKeyServer);
//		PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(clear);		
//		PrivateKey privKeyServer = kf.generatePrivate(keySpec);
//
//		cipher.init(Cipher.DECRYPT_MODE, privKeyServer);
//		byte[] plainText = cipher.doFinal(decodedData);
//		System.out.println("plain : " + new String(plainText));

		
		Signature signature = Signature.getInstance("SHA1withRSA");
		signature.initVerify(publicKeyCustomer);
		signature.update(decodedData);
		//sig.verify(decodedSignature);
		System.out.println(signature.verify(decodedSignature));
				
		String pubKeyEncoded = Base64.getEncoder().encodeToString(pubKey.getEncoded());
		String privKeyEncoded = Base64.getEncoder().encodeToString(privKey.getEncoded());
		System.out.println("pub = " + encodedPublicKeyCustomer);
		System.out.println("priv = " + privKeyEncoded);
		System.out.println("msg = " + stringToStore);
		System.out.println("pub key = " + pubKey.toString());
		System.out.println("priv key = " + privKey.toString());
		System.out.println("pub key encoded = " + DatatypeConverter.printHexBinary(pubKey.getEncoded()));
		System.out.println("priv key encoded = " + DatatypeConverter.printHexBinary(privKey.getEncoded()));

	}
}
