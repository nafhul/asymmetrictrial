package com.daksa.mms.rsa256;

import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.util.Base64;
import javax.crypto.Cipher;


/**
 *
 * @author Nafhul A
 */
public class Main {

	public static void main(String[] args) throws Exception {
		Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());

		byte[] input = "s1m4gr1b_g4nt3ng".getBytes();
		//Cipher cipher = Cipher.getInstance("RSA/None/PKCS1Padding", "BC");
		Cipher cipher = Cipher.getInstance("RSA/None/PKCS1Padding", "BC");
		SecureRandom random = SecureRandom.getInstanceStrong();
		KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA", "BC");

		generator.initialize(2048, random);

		KeyPair pair = generator.generateKeyPair();
		Key pubKey = pair.getPublic();
		Key privKey = pair.getPrivate();
		PublicKey publicKey = (PublicKey) pubKey;
		System.out.println(pubKey.toString());
		//System.out.println(privKey.toString());
		cipher.init(Cipher.ENCRYPT_MODE, pubKey, random);
		byte[] cipherText = cipher.doFinal(input);
		String stringToStore = Base64.getEncoder().encodeToString(cipherText);
		
		byte[] restoredBytes = Base64.getDecoder().decode(stringToStore);
		
		String pubKeyEncoded = Base64.getEncoder().encodeToString(pubKey.getEncoded());
		String privKeyEncoded = Base64.getEncoder().encodeToString(privKey.getEncoded());
		
		System.out.println(publicKey.equals(pubKey)); 
		System.out.println("pub = " + pubKeyEncoded);
		System.out.println("priv = " + privKeyEncoded);
		
		System.out.println("cipher: " + new String(cipherText));
		System.out.println("cipher String: " + Base64.getEncoder().encodeToString(cipherText));
		
		cipher.init(Cipher.DECRYPT_MODE, privKey);
		byte[] plainText = cipher.doFinal(cipherText);
		System.out.println("plain : " + new String(plainText));
//		System.out.println(new String(privKey.getEncoded()));
//		System.out.println("");
//		System.out.println("---");
//		System.out.println("");
//		System.out.println(new String(pubKey.getEncoded()));
	}
}
